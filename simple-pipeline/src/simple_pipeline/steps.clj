(ns simple-pipeline.steps
  (:require [lambdacd.steps.shell :as shell]
            [lambdacd-git.core :as git]))

(def repo "https://gitlab.com/rafasf/streaming-playground.git")

(defn wait-for-repo [_ ctx]
  (git/wait-for-git ctx
                    repo
                    :ref "refs/heads/master"
                    :ms-between-polls (* 60 1000)))

(defn clone [args ctx]
  (git/clone ctx repo (:revision args) (:cwd args)))

(defn compile-and-test [{cwd :cwd} ctx]
  (shell/bash ctx cwd "ls")
  (shell/bash ctx (str cwd "/apps/crime-feed") "lein uberjar"))

(defn build-image [{cwd :cwd} ctx]
  (shell/bash ctx
              (str cwd "/apps/crime-feed")
              "docker build . -t localhost:5000/crime-feed:0.0.1"))
