(ns simple-pipeline.pipeline
  (:use [lambdacd.steps.control-flow]
        [simple-pipeline.steps])
  (:require
    [lambdacd.steps.manualtrigger :as manualtrigger]))

(def pipeline-def
  `(
    (either
      manualtrigger/wait-for-manual-trigger
      wait-for-repo)
    (with-workspace
      clone
      compile-and-test
      build-image)))
