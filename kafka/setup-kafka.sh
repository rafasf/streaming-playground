#!/usr/bin/env bash

set -e

function kube_create() {
  kubectl create -f $1
}

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

echo "### Setting up DNS..."
kube_create ${__dir}/dns.yml

echo "### Setting up Kafka Service..."
kube_create ${__dir}/service.yml

echo "### Setting up Kafka PetSet..."
kube_create ${__dir}/kafka.yml
