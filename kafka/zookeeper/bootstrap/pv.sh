#!/bin/bash

__dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

path="${__dir}/data"
cat "${__dir}/bootstrap/pv-template.yml" | sed "s|/tmp/k8s-data|$path|" | kubectl create -f -
