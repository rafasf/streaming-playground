#!/usr/bin/env bash

set -e

function kube_create() {
  kubectl create -f $1
}

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

echo "### Setting up Kafka namespace..."
kube_create ${__dir}/namespace.yml

echo "Creating Persistent Volumes (zookeeper)..."
${__dir}/zookeeper/bootstrap/pv.sh

echo "Claiming Volumes (zookeeper)..."
kube_create ${__dir}/zookeeper/bootstrap/pvc.yml

echo "Setting up Zookeeper PetSet"
kube_create ${__dir}/zookeeper/zookeeper.yaml

echo "Creating Persistent Volumes (kafka)..."
${__dir}/bootstrap/pv.sh

echo "Claiming Volumes (kafka)..."
kube_create ${__dir}/bootstrap/pvc.yml

echo "### All claims so far..."
kubectl get pvc

echo "### Starting Zookeeper Service..."
kube_create ${__dir}/zookeeper/service.yml

echo "#### run ./setup-kafka.sh after zookeeper is up"
