(ns crime-feed.core
  (:gen-class)
  (:require [kafka-clj.produce :as kp]
            [clojure.data.csv :as csv]
            [clojure.data.json :as json]
            [clojure.java.io :as io]))

(defn publish [message topic producer]
  (kp/send-messages producer {} [{:topic topic :partition 0 :bts (.getBytes (json/write-str message))}]))

(defn read-crime-data []
  (with-open [in-file (io/reader "crime-data.csv")]
    (doall
      (csv/read-csv in-file))))

(defn add-field-name [lines]
  (map #(zipmap [:id :case-number :date :block :iucr :primary-type :description :location-description :arrest :domestic :beat :district :ward :community-area :fbi-code :x-coordinate :y-coordinate :year :updated-on :latitude :longitude :location]
               %1) lines))

(defn crimes []
  (->> (read-crime-data)
       (add-field-name)))

(defn publish-crimes []
  (let [some-crimes (crimes)
        producer (kp/producer "kafka-0.broker" 9092 {:acks 1})]
    (println "start sending crimes...")
    (doseq [crime (cycle some-crimes)]
      (println "sending: " (crime :case-number) "...")
      (publish crime "crimes" producer)
      (Thread/sleep 1000))))

(defn -main [& args]
  (publish-crimes))
