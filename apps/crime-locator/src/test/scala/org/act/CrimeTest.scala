package org.act
import org.scalatest._

import scala.collection.immutable.HashMap

class CrimeTest extends FlatSpec with Matchers {
  "A map with the correct values" should "be mapped to Crime" in {
    val crime: Crime = Crime.from(HashMap(
      "location-description" -> "one",
      "description" -> "blah",
      "arrest" -> "true",
      "date" -> "2010-03-02"))

    crime.locationDescription should equal("one")
    crime.description should equal("blah")
    crime.arrest should be(true)
    crime.date should equal("2010-03-02")
  }
}
