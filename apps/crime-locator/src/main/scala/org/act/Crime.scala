package org.act

import scala.util.Try

object Crime {
  def from(map: Map[String, Any]) = {
    new Crime(
      map.getOrElse("location-description", "").asInstanceOf[String],
      map.getOrElse("description", "").asInstanceOf[String],
      Try(map.getOrElse("arrest", "").asInstanceOf[String].toBoolean).getOrElse(false),
      map.getOrElse("date", "1999-01-01").asInstanceOf[String])
  }
}

class Crime(val locationDescription: String, val description: String, val arrest: Boolean, val date: String) {
  override def toString = s"${this.locationDescription}, ${this.description}, ${this.arrest}, ${this.date}"

  /*./
Some(Map(
y-coordinate -> 1925823,
updated-on -> 10/25/2015 03:55:25 PM,
x-coordinate -> 1129571,
location-description -> STREET,
domestic -> false,
location -> (41.95274479, -87.799092807),
latitude -> 41.95274479,
description -> THEFT OF LOST/MISLAID PROP,
community-area -> 17,
case-number -> HY466732,
arrest -> true,
year -> 2015,
longitude -> -87.799092807,
block -> 069XX W IRVING PARK RD,
ward -> 38,
id -> 10278428,
date -> 10/18/2015 12:00:00 PM,
beat -> 1632,
fbi-code -> 11,
district -> 016,
iucr -> 1220,
primary-type -> DECEPTIVE PRACTICE))
 */

}
